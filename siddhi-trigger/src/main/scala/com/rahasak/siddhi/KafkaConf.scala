package com.rahasak.siddhi

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait KafkaConf {
  // config object
  val kafkaConf = ConfigFactory.load("kafka.conf")

  // kafka conf
  lazy val kafkaAddr = Try(kafkaConf.getString("kafka.addr")).getOrElse("dev.localhost:9092")
  lazy val kafkaGroup = Try(kafkaConf.getString("kafka.group")).getOrElse("aplosg")
  lazy val kafkaTopic = Try(kafkaConf.getString("kafka.topic")).getOrElse("aplos")
  lazy val kafkaRegistrarApiTopic = Try(kafkaConf.getString("kafka.registrarapi-topic")).getOrElse("registrarapi")
}

