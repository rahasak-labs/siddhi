name := "siddhi-trigger"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  Seq(
    "com.typesafe.akka"         %% "akka-actor"       % "2.4.14",
    "com.typesafe.akka"         %% "akka-slf4j"       % "2.4.14",
    "org.apache.cassandra"      % "cassandra-all"     % "3.11.0",
    "org.apache.kafka"          % "connect-api"       % "0.9.0.0",
    "org.apache.kafka"          % "connect-runtime"   % "0.9.0.0",
    "org.scalatest"             % "scalatest_2.11"    % "2.2.1"               % "test"
  )
}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case x => MergeStrategy.first
}
